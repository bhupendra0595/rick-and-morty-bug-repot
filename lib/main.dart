import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // required this widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MyHomPage();
  }
}

class MyHomPage extends StatefulWidget {
  const MyHomPage({Key? key}) : super(key: key);

  @override
  _MyHomPageState createState() => _MyHomPageState();
}

class _MyHomPageState extends State<MyHomPage> {
  final HttpLink rickAndMortyHttpLink =
      HttpLink('https://rickandmortyapi.com/graphql');
  late final ValueNotifier<GraphQLClient> Client;
  @override
  void initState() {
    Client = ValueNotifier(
      GraphQLClient(
        link: rickAndMortyHttpLink,
        cache: GraphQLCache(
          store: InMemoryStore(),
        ),
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(
      client: Client,
      child: MaterialApp(
        title: 'Material App',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Rick n Morty'),
            backgroundColor: Colors.black,
          ),
          body: const RickAndMortyChacactersListWidget(),
        ),
      ),
    );
  }
}

class RickAndMortyChacactersListWidget extends StatefulWidget {
  const RickAndMortyChacactersListWidget({
    Key? key,
  }) : super(key: key);

  @override
  _RickAndMortyChacactersListWidgetState createState() =>
      _RickAndMortyChacactersListWidgetState();
}

class _RickAndMortyChacactersListWidgetState
    extends State<RickAndMortyChacactersListWidget> {
  final GraphQLClient _graphQLClient = GraphQLClient(
    link: HttpLink('https://rickandmortyapi.com/graphql'),
    cache: GraphQLCache(
      store: InMemoryStore(),
    ),
  );

  final ValueNotifier<bool> _isLoading = ValueNotifier(false);
  final ValueNotifier<bool> _isError = ValueNotifier(false);

  Future<void> _getAllCharacters() async {
    _isLoading.value = true;
    final QueryResult result = await _graphQLClient.query(
      QueryOptions(document: gql(rickCharacters)),
    );
    if (result.hasException) {
      _isError.value = true;
    } else {
      _isError.value = false;
      _characterLists = List.from(result.data!['characters']['results'])
          .map((char) => RickNMortyCharacterModel.fromJson(char))
          .toList();
    }
    _isLoading.value = false;
  }

  List<RickNMortyCharacterModel> _characterLists = [];

  @override
  void initState() {
    _getAllCharacters();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: _isLoading,
      builder: (context, isLoading, child) => isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : child!,
      child: ValueListenableBuilder<bool>(
        valueListenable: _isError,
        builder: (context, hasError, child) => hasError
            ? const Center(
                child: Text(
                  'Something went Wrong..!!',
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 36,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              )
            : ListView.separated(
                separatorBuilder: (context, index) => const Divider(),
                itemBuilder: (context, index) => GestureDetector(
                  onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CharacterDetailsWidget(
                          rickNMortyCharacterModel: _characterLists[index],
                        ),
                      )),
                  child: Row(
                    children: [
                      Image.network(
                        _characterLists[index].image,
                        height: 50,
                        width: 50,
                      ),
                      Column(
                        children: [
                          Text(_characterLists[index].name),
                          Text(
                              'location : ${_characterLists[index].location} : status : ${_characterLists[index].status}'),
                        ],
                      )
                    ],
                  ),
                ),
                itemCount: _characterLists.length,
              ),
      ),
    );
  }
}

class CharacterDetailsWidget extends StatelessWidget {
  final RickNMortyCharacterModel rickNMortyCharacterModel;
  const CharacterDetailsWidget({
    Key? key,
    required this.rickNMortyCharacterModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(rickNMortyCharacterModel.name),
      ),
      body: Column(
        children: [
          Image.network(rickNMortyCharacterModel.image),
          const SizedBox(height: 10),
          Text('Status : ${rickNMortyCharacterModel.status}'),
          const Divider(),
          Text('Gender : ${rickNMortyCharacterModel.gender}'),
          const Divider(),
          Text('species : ${rickNMortyCharacterModel.species}'),
          const Divider(),
          Text('location : ${rickNMortyCharacterModel.location} '),
          const Divider(),
          Text('Dimension : ${rickNMortyCharacterModel.dimension} '),
        ],
      ),
    );
  }
}

class RickNMortyCharacterModel {
  final String name;
  final String status;
  final String species;
  final String gender;
  final String image;
  final String location;
  final String dimension;

  RickNMortyCharacterModel({
    required this.name,
    required this.status,
    required this.species,
    required this.gender,
    required this.image,
    required this.location,
    required this.dimension,
  });
  factory RickNMortyCharacterModel.fromJson(dynamic) {
    return RickNMortyCharacterModel(
      name: dynamic['name'],
      status: dynamic['status'],
      species: dynamic['species'],
      gender: dynamic['gender'],
      image: dynamic['image'],
      location: dynamic['location']['name'],
      dimension: dynamic['location']['dimension'] ?? '',
    );
  }
}

const rickCharacters = '''
 query{
  characters(page:1){
    results{
      name
      status
      species
      gender
      image
      location{
        name
        dimension
      }
    }
  }
}
  ''';
